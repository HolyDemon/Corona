-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here

ancho = display.contentWidth
alto = display.contentHeight

local composer = require "composer"

local composerOptions = {
    effect = "fade",
    time = 500,
}

composer.gotoScene( "scenes.mainMenu", composerOptions )
