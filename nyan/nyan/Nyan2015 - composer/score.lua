----------------------------------------------------------------------------------
--
-- SceneScore.lua
--
----------------------------------------------------------------------------------

local composer = require ("composer")
local scene = composer.newScene()

-- createScene
function scene:create( event )
	
	print "SceneScore createScene"
	local group = self.view
	
end

-- enterScene
function scene:show( event )
	
	print "SceneScore enterScene"
	local group = self.view
	local phase = event.phase

    if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then    

        local bg = display.newImage("images/score_bg.png",0,0,true)
        bg.anchorX = 0
        bg.anchorY = 0
        local tScore = display.newText( "puntaje : "..score, _W/2, 300, native.systemFontBold, 80 )
        tScore:setTextColor( 255, 255, 255)
        local home = display.newImage("images/score_home.png",_W/2,_H*0.5,true)
        local sMusic = audio.loadStream ("sounds/sound_bg1.mp3")
        local sSound = audio.loadSound ("sounds/sound_select.wav")	
        audio.play(sMusic, { channel=3, loops=-1, fadein=3000 } )

        function fPlay(event)
            if(event.phase == "began") then
                audio.stop(3)
                composer.gotoScene("mainMenu","fade",700)
                 -- detenemos el canal
                audio.play(sSound)
            end
        end
        home:addEventListener("touch", fPlay)
        group:insert(bg)
        group:insert(home)
        group:insert(tScore)
    
    end
	
end

-- exitScene
function scene:hide( event )
	
	print "SceneScore exitScene"
	local group = self.view
	composer.removeScene( "score" )
	composer.removeScene( "mision1" )

end

-- destroyScene
function scene:destroy( event )

	print "SceneScore destroyScene"
	local group = self.view

end

-- sceneEventListeners
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

-- end