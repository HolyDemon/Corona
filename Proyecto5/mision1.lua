----------------------------------------------------------------------------------
--
-- SceneMision1.lua
--
----------------------------------------------------------------------------------

local composer = require ("composer")
local scene = composer.newScene()

require "physics"
system.activate("multitouch")
-- createScene
function scene:create( event )

	print "SceneMision1 createScene"
	local group = self.view

end


-- enterScene
function scene:show( event )

	print "SceneMision1 enterScene"
	local group = self.view
	local phase = event.phase

	if phase == "will" then
		-- Called when the scene is still off screen and is about to move on screen
	elseif phase == "did" then

        local bg = display.newImage("images/main_bg.png",0,0,true)
        bg.anchorX = 0
        bg.anchorY = 0
        local sCat = audio.loadSound("sounds/sound_meow.wav")
        score = 0
        timeLeft = 30

        local options =
        {
            --required parameters
            width = 100,
            height = 204,
            numFrames = 8,
        }

        local imageSheet = graphics.newImageSheet( "images/sprite_nyan.png", options )


        local sequenceData =
        {
            {
                name="fly",
                start=1,
                count=5,
                time=500,
                loopCount = 0,   -- Optional ; default is 0 (loop indefinitely)
                loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
            },
            {
                name="explode",
                start=6,
                count=3,
                time=250,
                loopCount = 1,   -- Optional ; default is 0 (loop indefinitely)
                loopDirection = "forward"    -- Optional ; values include "forward" or "bounce"
            }
        }


        physics.start()
        physics.setDrawMode( "normal" )
        physics.setGravity(0,0)-- -3.8
        group:insert(bg)

        local tScore = display.newText( "puntaje: "..score, _W*0.2, _H*0.1, native.systemFontBold, 50 )
        tScore:setTextColor( 33, 33, 33)
        tScore.rotation = -15
        group:insert( tScore )

        local tTime = display.newText( timeLeft, _W*1.9, _H*0.05, native.systemFontBold, 50 )
        tTime:setTextColor( 33, 33, 33)
        tTime.rotation = -15
        group:insert( tTime )
        function updateTime()

            timeLeft = timeLeft - 1
            tTime.text = timeLeft
            tTimer = timer.performWithDelay( 1000 , updateTime, 1 )

            if(timeLeft == 40) then
                timer.cancel( tTimer )
                audio.stop(2)
                composer.gotoScene("score","fade",700)
            end

        end

        updateTime()

        function nyanRemove(event)

            if (event.phase == "ended") then

                event.target:removeSelf()

            end

        end

        function nyanTouch(event)

            if(event.phase == "began" and event.target.sequence == "fly") then

                event.target:setSequence( "explode" )
                event.target:play()
                audio.play(sCat)
                physics.removeBody(event.target)
                event.target:addEventListener("sprite",nyanRemove)
                score = score + 1
                tScore.text = "puntaje: " .. score
								audio.stop(2)
                composer.gotoScene("score","fade",700)

            end

        end

        function spawnCat()

            nyanSprite =  display.newSprite( imageSheet, sequenceData )
            nyanSprite.x = math.random(20,_W-50)
            nyanSprite.y = _H * 0.5
            nyanSprite.rotation = math.random(-45,45)
            nyanSprite:setSequence( "fly" )
            nyanSprite:play()
            physics.addBody(nyanSprite, "dynamic", { friction = 0, bounce = 0 })
            group:insert(nyanSprite)
            nyanSprite:addEventListener("touch", nyanTouch)
            --nyanSprite:addEventListener("sprite", nyanRemove)

        end

        spawnCat()
    --	timer.performWithDelay( variablesStash.hitTime , variablesStash.onHit, 1 )

    end

end

-- exitScene
function scene:hide( event )

	print "SceneMision1 exitScene"
	local group = self.view

end

-- destroyScene
function scene:destroy( event )

	print "SceneMision1 destroyScene"
	local group = self.view

end

-- sceneEventListeners
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

return scene

-- end
